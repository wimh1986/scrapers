# -*- coding: utf-8 -*-

from scrapy.loader.processors import MapCompose, Join
from scrapy.loader import ItemLoader
from properties.items import PropertiesItem

import datetime
import socket
import urllib
import scrapy


class BasicSpider(scrapy.Spider):
    name = 'basic'
    allowed_domains = ['www.gumtree.com']
    start_urls = ['https://www.gumtree.com/p/property-for-sale/now-fixed-price-5-bedroom-beautiful-family-house.-blanefield.-just-past-milngavie-a-must-see-/1264705724',
                  'https://www.gumtree.com/p/property-for-sale/flat-apartment-1-bed-in-pl4./1254696997',
                  'https://www.gumtree.com/p/property-for-sale/-affordable-holiday-home-ownership-scottish-borders-/1254959186',]

    start_urls = [i.strip() for i in
                  open('todo_urls.txt').readlines()]


    def parse(self, response):
        """This function parses a property page.

        @url https://www.gumtree.com/p/property-for-sale/now-fixed-price-5-bedroom-beautiful-family-house.-blanefield.-just-past-milngavie-a-must-see-/1264705724
        @returns items 3
        @scrapes title price description image_urls
        @scrapes url project spider date
        """

        l = ItemLoader(item=PropertiesItem(), response=response)
        
        # load fields from xpath
        l.add_xpath('title', '//h1[@itemprop="name"][1]/text()',
                    MapCompose(str.strip, str.title))
        l.add_xpath('price', '(//*[@itemprop="price"]/@content)[1]',
                    MapCompose(lambda i: i.replace(',', ''), float), re='[,.0-9]+')
        l.add_xpath('description', '//*[text()="Description"]/following-sibling::*[1][@itemprop="description"]/text()',
                    MapCompose(str.strip), Join())
        l.add_xpath('image_urls', '//*[@itemprop="image"][1]/@src',
                    MapCompose(lambda i: urllib.parse.urljoin(response.url, i)))

        # housekeeping fields
        l.add_value('url', response.url),
        l.add_value('project', self.settings.get('BOT_NAME')),
        l.add_value('spider', self.name),
        l.add_value('date', datetime.datetime.now())

        return l.load_item()
