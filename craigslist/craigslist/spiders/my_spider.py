
import scrapy


class MySpider(scrapy.Spider):
    name = "craigslist"
    start_urls = ["https://brussels.craigslist.org/search/cla?lang=nl"]

    def parse(self, response):
        for title in response.xpath("//li[@class='result-row']/p"):
            yield {
                'title':title.xpath("a/text()").extract_first()
                }

