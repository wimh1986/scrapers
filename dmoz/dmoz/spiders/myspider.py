
import scrapy

class DmozSpider(scrapy.Spider):
    name = "dmoz"
    start_urls = [
        "http://dmoztools.net/Computers/Programming/Languages/Python/Books/",
        "http://dmoztools.net/Computers/Programming/Languages/Python/Resources/"
        ]


    def parse(self, response):
        for site in response.xpath('//div[@class="title-and-desc"]'):
            yield{
                'name':site.xpath('./a/div/text()').extract_first(),
                'url':site.xpath('./a/@href').extract_first()
                }
