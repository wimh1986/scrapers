from scrapy.item import Item, Field
from scrapy.loader.processors import MapCompose, Join, TakeFirst
from w3lib.html import remove_tags


class AsosItem(Item):
    # xpath fields
    title = Field(
            input_processor=MapCompose(remove_tags),
            output_processor=TakeFirst()
    )
  
    price = Field(
            input_processor=MapCompose(remove_tags),
            output_processor=TakeFirst()
    )

    brand_category = Field(
    )
  
    product_details = Field(
    )
  
    product_code = Field(
            input_processor=MapCompose(remove_tags),
            output_processor=TakeFirst()
    )
 
    brand_description = Field(
    )

    image_urls = Field(
    )

    gender = Field(output_processor=TakeFirst())
    url = Field(output_processor=TakeFirst())
    project = Field(output_processor=TakeFirst())
    spider = Field(output_processor=TakeFirst())
    datetime = Field(output_processor=TakeFirst())
