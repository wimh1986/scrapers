# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

import argparse
import json
import io
import avro.schema
import avro.datafile 
from avro.io import DatumWriter

from google.cloud import pubsub

from scrapy.conf import settings
from scrapy.exceptions import DropItem


# AVRO schema
SCHEMA = avro.schema.Parse(json.dumps({
 "namespace"    : "asos.avro",
 "type"         : "record",
 "name"         : "asos",
 "fields"       : [
    {"name": "title",               "type": "string"},
    {"name": "price",               "type": "string"},
    {"name": "brand_category",      "type": {"type": "array", "items": "string"}},
    {"name": "product_details",     "type": {"type": "array", "items": "string"}},
    {"name": "product_code",        "type": "string"},
    {"name": "brand_description",   "type": {"type": "array", "items": "string"}},
    {"name": "image_urls",          "type": {"type": "array", "items": "string"}},
    {"name": "gender",              "type": "string"},
    {"name": "url",                 "type": "string"},
    {"name": "project",             "type": "string"},
    {"name": "spider",              "type": "string"},
    {"name": "datetime",            "type": "string"},
 ]
}))

class PubSubPipeline(object):
    def __init__(self):
        # PubSub
        self.topic = settings['PUBSUB_TOPIC']
        self.publisher = pubsub.PublisherClient()

    def encode(self, schema_file, data):
        raw_bytes = None
        data = data
        schema = schema_file
        try:
            writer = avro.io.DatumWriter(schema)
            bytes_writer = io.BytesIO()
            encoder = avro.io.BinaryEncoder(bytes_writer)
            writer.write(data, encoder)
            raw_bytes = bytes_writer.getvalue()
        except Exception as e1:
            logger.warn("Encore encoding data: %s" % e1)
        return raw_bytes

    def send(self, raw_bytes):  
        try:
            self.publisher.publish(self.topic, raw_bytes)
        except Exception as e1:
            logger.warn("Error send message to pubsub: %s" % e1)

    def process_item(self, item, spider):
        valid = True
        ditem = dict(item)
        for data in ditem:
            if not data:
                valid = False
                logger.warn("Missing %s" % data)
        raw_bytes = self.encode(SCHEMA, ditem)
        logger.debug("raw_bytes is of type: %s" % type(raw_bytes))
        if raw_bytes is not None:
            self.send(raw_bytes)
        else:
            logger.warn("raw_bytes is None")
