# -*- coding: utf-8 -*-
# Scrapy settings for asos project

BOT_NAME = 'asos'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36i'

SPIDER_MODULES = ['asos.spiders']
NEWSPIDER_MODULE = 'asos.spiders'

# Log Settings
LOG_LEVEL = 'DEBUG'
LOGSTATS_INTERVAL = 10

# Stats

# Performance
CONCURRENT_REQUESTS = 1
DOWNLOAD_DELAY = 3

ROBOTSTXT_OBEY = True


ITEM_PIPELINES = {
    'asos.pipelines.PubSubPipeline': 300,
}
PUBSUB_TOPIC='projects/localdev-174318/topics/asos'


# Cache
HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 100000
HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.LeveldbCacheStorage'
HTTPCACHE_POLICY = 'scrapy.extensions.httpcache.RFC2616Policy'
