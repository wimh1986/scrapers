
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader.processors import MapCompose, Join
from scrapy.loader import ItemLoader
from scrapy.http import Request
from asos.items import AsosItem

import datetime
import socket
import urllib
import scrapy


class AsosSpider(CrawlSpider):
    name = 'asos'
    allowed_domains = ['www.asos.com']
    start_urls = ['http://www.asos.com/women/new-in-clothing/cat/?cid=2623&pgesize=36&sort=freshness',
                  'http://www.asos.com/men/new-in-clothing/cat/?cid=6993&pgesize=36&sort=freshness']

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//*[@class="next"]')),
        Rule(LinkExtractor(restrict_xpaths='//*[@class="product-container interactions"]'),
                callback='parse_item')
        )

    def parse_item(self,response):
        """This function parses a product page.

        @url 
        @returns items 
        @scrapes 
        """

        l = ItemLoader(item=AsosItem(), response=response)

        # load fields from xpath
        l.add_xpath('title', '//*[@class="product-hero"]//h1/text()')
#        l.add_xpath('price', '//*[@class="product-price"]//*[@class="current-price"]/text()')
        l.add_value('price', '5')
        l.add_xpath('brand_category', '//*[@class="product-description"]//*[not(ancestor-or-self::ul)]/text()')
        l.add_xpath('product_details', '//*[@class="product-description"]//*[(ancestor-or-self::ul)]/text()')
        l.add_xpath('product_code', '//*[@class="product-code"]/span/text()')
        l.add_xpath('brand_description', '//*[@class="brand-description"]/span/text()')
        l.add_xpath('image_urls', '//*[@class="thumbnails"]//@src')

        # other
        l.add_value('gender', response.url.split("/")[3])

        # load housekeeping fields
        l.add_value('url', response.url)
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('datetime', datetime.datetime.utcnow().isoformat())

        return l.load_item()
